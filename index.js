var loadLib = function(lib){
    return require(__dirname+'/libs/'+lib+'.js')
}
var s = loadLib('process')(process,__dirname)
var config = loadLib('config')(s)
loadLib('basic')(s,config)
loadLib('scan')(s,config)
loadLib('proxy')(s,config)
loadLib('webServer')(s,config)
loadLib('vpnClient')(s,config)
loadLib('electron')(s,config)
if(process.argv[2]){
    switch(process.argv[2]){
        case'scan':
            s.getAllCamerasInNetwork()
        break;
    }
}
module.exports = s
