window.config = ejsData.config
const shell = require('electron').shell;
var webHost = 'http://127.0.0.1:' + ejsData.webPort + '/'
var getJSON = function(pointer,callback){
    $.get(webHost + pointer,function(data){
        callback(data)
    })
}
var setScanStatus = function(data){
    var currentlyScanningHider = $('.currentlyScanningHider')
    if(data.currentlyScanning){
        currentlyScanningHider.show()
    }else{
        currentlyScanningHider.hide()
    }
    $('.numberOfDevices').text(data.numberOf.devices)
    $('.numberOfRtspDevices').text(data.numberOf.rtspDevices)
    $('.numberOfHttpDevices').text(data.numberOf.httpDevices)
}
var proxiedDevices = $('#proxied-devices')
var proxiedDevicesTableBody = proxiedDevices.find('table tbody')
var networkInterfaces = $('#network-interfaces')
var networkInterfacesTableBody = networkInterfaces.find('table tbody')
var drawProxiedDevicesTable = function(data){
    var html = ''
    $.each(data.allDevices,function(ipLastDigit,device){
        html += buildDeviceTableRow(device)
    })
    proxiedDevicesTableBody.html(html)
    $('[data-toggle="tooltip"]').tooltip()
}
var buildDeviceTableRow = function(device){
    var html = ''
    html += `<tr>\
<td><b>${device.ip}</b></td>`
    if(device.proxyHttpPort){
        html += `<td><a href="http://${device.ip}:${config.defaultHostPort}" new-window target="_blank" data-toggle="tooltip" title="Open Direct" class="btn btn-sm btn-default">${config.defaultHostPort}</a> <a href="http://127.0.0.1:${device.proxyHttpPort}" data-toggle="tooltip" title="Open Proxy" new-window target="_blank" class="btn btn-sm btn-default">${device.proxyHttpPort}</a></td>`
    }else{
        html += `<td>Port Closed</td>`
    }
    ///
    if(device.proxyRtspPort){
        html += `<td><a href="rtsp://${device.ip}:${config.defaultRtspPort}" click-to-copy="href" data-toggle="tooltip" title="Click to Copy Address" target="_blank" class="btn btn-sm btn-primary">${config.defaultRtspPort}</a> <a href="rtsp://127.0.0.1:${device.proxyRtspPort}" click-to-copy="href" data-toggle="tooltip" title="Click to Copy Address" target="_blank" class="btn btn-sm btn-primary">${device.proxyRtspPort}</a></td>`
    }else{
        html += `<td>Port Closed</td>`
    }
    ///
    html += `</tr>`
    return html
}
var drawNetworkInterfacesTable = function(data){
    var html = ''
    $.each(data.interfaces,function(name,interface){
        html += buildNetworkInterfaceTableRow(name,interface)
    })
    networkInterfacesTableBody.html(html)
    $('[data-toggle="tooltip"]').tooltip()
}
var buildNetworkInterfaceTableRow = function(name,interface){
    var html = ''
    html += `<tr>`
    html += `<td><b>${name}</b></td>`
    html += `<td>`
    html += `<ul>`
    $.each(interface,function(n,ip){
        html += `<li>${ip.address}</li>`
    })
    html += `</ul>`
    html += `</td>`
    html += `</tr>`
    return html
}
$(document).ready(function(){
    $('body')
    .on('click','[get-json]',function(){
        var pointer = $(this).attr('get-json')
        getJSON(pointer,function(data){
            switch(pointer){
                case'scan':case'scanResults':
                    drawProxiedDevicesTable(data)
                    $('#tabs-icons-text-1-tab').click()
                break;
                case'interfaces':
                    drawNetworkInterfacesTable(data)
                    $('#tabs-icons-text-2-tab').click()
                break;
                case'vpnCreate':
                    //launches windows tool
                break;
            }
        })
    })
    .on('click','[click-to-copy]',function(e){
        var pointer = $(this).attr('click-to-copy')
        var text
        switch(pointer){
            case'href':
                e.preventDefault()
                text = $(this).attr('href')
            break;
        }
        var $temp = $("<input>")
        $("body").append($temp)
        $temp.val(text).select()
        document.execCommand("copy")
        $temp.remove()
    })
    getJSON('interfaces',function(data){
        drawNetworkInterfacesTable(data)
    })
    $('#form-vpn-create').submit(function(e){
        e.preventDefault()
        console.log($(this).serializeObject())
        $.post(webHost + 'vpnCreate',$(this).serializeObject(),function(data){
            console.log(data)
        })
        return false;
    })
})
$(document).on('click', 'a[href^="http"]', function(event) {
    event.preventDefault()
    shell.openExternal(this.href)
})
// Socket.IO Handles (WebSocket)
var webSocket = io(webHost,{
    transports: ['websocket'],
    forceNew: false
})
webSocket.on('f',function(data){
    switch(data.f){
        case'currentlyScanningDeviceInfo':
            $('#json-output').text(JSON.stringify(data,null,3))
        break;
        case'scannedNetwork':
            drawProxiedDevicesTable(data)
        break;
        case'currentlyScanningStatus':
            setScanStatus(data)
        break;
    }
})
