var express = require('express')
var http = require('http')
var app = express()
var io = new (require('socket.io'))()
var bodyParser = require('body-parser')
var exec = require('child_process').exec
module.exports = function(s,config){
    // HTTP Config
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    // HTTP Handles
    app.use(function (req,res,next){
        res.setHeader('Content-Type', 'application/json')
        next()
    })
    app.get('/', function (req,res){
        var endData = s.getScannerInfo()
        if(!s.currentlyScanning){
            endData.msg = "Visit '/scan.json' to begin scanning."
        }
        res.end(s.prettyPrint(endData))
    })
    app.get('/scan', function (req,res){
        var endData = {
            ok: false
        }
        if(!s.currentlyScanning){
            s.getAllCamerasInNetwork()
            endData.ok = true
        }else{
            endData.msg = "Already scanning"
        }
        endData = Object.assign(endData,s.getScannerInfo())
        res.end(s.prettyPrint(endData))
    })
    app.get('/scanResults', function (req,res){
        var endData = {
            ok: false
        }
        endData.ok = true
        endData = Object.assign(endData,s.getScannerInfo())
        res.end(s.prettyPrint(endData))
    })
    app.get('/interfaces', function (req,res){
        var endData = {
            ok: false
        }
        endData.ok = true
        endData.interfaces = s.loadNetworkInterfaces()
        res.end(s.prettyPrint(endData))
    })
    app.all('/vpnCreate', function (req,res){
        var endData = {
            ok: false
        }
        if(s.isWindows){
            if(s.isElectronApp){
                exec('rasphone')
                endData.ok = true
                endData.msg = `Launched rasphone.`
            }else{
                endData.ok = false
                endData.msg = `Windows host must create a VPN connection manually or use the Kensho interface to launch the configuration tool.`
            }
            res.end(s.prettyPrint(endData))
        }else{
            var user = s.getPostData(req,'user')
            var pass = s.getPostData(req,'pass')
            var domain = s.getPostData(req,'domain')
            var name = s.getPostData(req,'name')
            var enabled = s.getPostData(req,'enabled')
            var type = s.getPostData(req,'type') || 'pptp'
            switch(type){
                case'pptp':
                    s.createPptpVpnConnection(
                        domain,
                        user,
                        pass,
                        name,
                        function(err){
                            if(enabled)s.startPptpVpnConnection(name)
                            endData.ok = true
                            endData.err = err
                            endData.interfaces = s.loadNetworkInterfaces()
                            res.end(s.prettyPrint(endData))
                        }
                    )
                break;
                default:
                    endData.msg = `Connection Type not available.`
                    res.end(s.prettyPrint(endData))
                break;
            }
        }
    })
    app.all('/vpnStart', function (req,res){
        var endData = {
            ok: false
        }
        if(s.isWindows){
            if(s.isElectronApp){
                exec('rasphone')
                endData.ok = true
                endData.msg = `Launched rasphone.`
            }else{
                endData.ok = false
                endData.msg = `Windows host must create a VPN connection manually or use the Kensho interface to launch the configuration tool.`
            }
            res.end(s.prettyPrint(endData))
        }else{
            var name = s.getPostData(req,'name')
            switch(s.getPostData(req,'type')){
                case'pptp':
                    s.startPptpVpnConnection(
                        name,
                        function(err){
                            endData.ok = true
                            endData.err = err
                            endData.interfaces = s.loadNetworkInterfaces()
                            res.end(s.prettyPrint(endData))
                        }
                    )
                break;
                default:
                    endData.msg = `Connection Type not available.`
                    res.end(s.prettyPrint(endData))
                break;
            }
        }
    })
    var server = http.createServer(app)
    server.listen(config.webPanelPort,null,function(){
        console.log(`Kensho running on port ${config.webPanelPort}!`)
    })
    // Socket.IO Handles (WebSocket)
    io.attach(server,{
        transports: ['websocket']
    })
    io.on('connection', function(socket){
        s.sendCurrentlyScanningStatus()
        var txData = s.getScannerInfo()
        txData.f = 'scannedNetwork'
        s.sendToConnectedClients(txData)
    })
    s.sendToConnectedClients = function(data){
        io.emit('f',data)
    }
}
