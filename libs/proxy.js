var net = require('net')
module.exports = function(s,config){
    s.attachProxy = function(localPort,remoteHost,remotePort){
        //original proxy code from : https://gist.github.com/kfox/2313683
        var LOCAL_PORT  = localPort
        var REMOTE_PORT = remotePort
        var REMOTE_ADDR = remoteHost
        var server = net.createServer(function (localsocket) {
            // console.log("Proxy Connecting : 127.0.0.1:%d to %s:%d", LOCAL_PORT, REMOTE_ADDR, REMOTE_PORT)
            var remotesocket = new net.Socket()

            remotesocket.connect(REMOTE_PORT, REMOTE_ADDR)

            localsocket.on('connect', function (data) {
                // console.log("Proxy Connected : 127.0.0.1:%d to %s:%d", LOCAL_PORT, REMOTE_ADDR, REMOTE_PORT)
            })

            localsocket.on('data', function (data) {
                var flushed = remotesocket.write(data)
                if (!flushed) {
                    localsocket.pause()
                }
            })

            remotesocket.on('data', function(data) {
                var flushed = localsocket.write(data)
                if (!flushed) {
                    remotesocket.pause()
                }
            })

            localsocket.on('drain', function() {
                remotesocket.resume()
            })

            remotesocket.on('drain', function() {
                localsocket.resume()
            })

            localsocket.on('close', function(had_error) {
                remotesocket.end()
            })

            remotesocket.on('close', function(had_error) {
                localsocket.end()
            })

        })

        server.listen(LOCAL_PORT)

        return server
    }
}
