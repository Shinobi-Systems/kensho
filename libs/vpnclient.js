var fs = require('fs')
var exec = require('child_process').exec
var execSync = require('child_process').execSync
module.exports = function(s,config){
    var checkForPppoeConnection = function(){
        var ifaces = s.loadNetworkInterfaces()
        var pppoeInterfaceFound = Object.keys(ifaces).indexOf('ppp') > -1
        if(pppoeInterfaceFound){
            return true
        }
        return false
    }
    var checkPppChapSecrets = function(domain,username,password){
        if(s.isWindows){
            console.log('Windows currently cannot use `checkPppChapSecrets`')
        }else{
            var secretsFile = execSync('cat /etc/ppp/chap-secrets')
            if(secretsFile.indexOf(`${domain}\\${username}    PPTP    ${password}     *`) > -1){
                return true
            }
        }
        return false
    }
    s.startPptpVpnConnection = function(name,callback){
        callback = callback || function(){}
        exec(`poff ${name}`,function(err,data){
            exec(`pon ${name}`,function(err,data){
                if(err)console.log(err)
                // console.log(data.toString())
                callback(err)
            })
        })

    }
    var createPptpPeerFile = function(domain,username,password,name,callback){
        var peerFileLocation = `/etc/ppp/peers/${name}`
        fs.unlink(peerFileLocation,function(err){
            var peerFileBody = `pty "pptp ${domain} --nolaunchpppd"\n`
                peerFileBody += `name ${username}\n`
                peerFileBody += `password ${password}\n`
                peerFileBody += `remotename PPTP\n`
                if(config.vpnPptpServerRequireMppe128)peerFileBody += `require-mppe-128\n`
                peerFileBody += `file /etc/ppp/options.pptp\n`
                peerFileBody += `ipparam ${name}`
            fs.writeFile(peerFileLocation,peerFileBody,'utf8',function(err){
                if(err)console.log(err)
                callback()
            })
        })
    }
    s.createPptpVpnConnection = function(domain,username,password,name,callback){
        callback = callback || function(){}
        if(s.isWindows){
            exec('rasphone')
            callback('Windows users must create VPN connections manually.')
        }else{
            if(checkForPppoeConnection() === false){
                if(config.vpnPptpServerForceRecreateConnection || checkPppChapSecrets(domain,username,password) === false){
                    var chapSecretLocation = "/etc/ppp/chap-secrets"
                    var chapSecretBody = `${domain}\\${username}    PPTP    ${password}     *`
                    fs.unlink(chapSecretLocation,function(err){
                        fs.writeFile(chapSecretLocation,chapSecretBody,'utf8',function(err){
                            if(err)console.log(err)
                            createPptpPeerFile(domain,username,password,name,function(){
                                callback()
                            })
                        })
                    })
                }else{
                    callback()
                }
            }else{
                callback(new Error('vpnclient : already created : ',username,domain))
            }
        }
    }
    if(
        config.vpnPptpServerAddress &&
        config.vpnPptpServerAddress !== '' &&
        config.vpnPptpServerUsername &&
        config.vpnPptpServerUsername !== '' &&
        config.vpnPptpServerPassword &&
        config.vpnPptpServerPassword !== '' &&
        config.vpnPptpServerName &&
        config.vpnPptpServerName !== ''
    ){
        console.log('Auto-Connect PPTP VPN on Start : ' + config.vpnPptpServerName)
        s.createPptpVpnConnection(
            config.vpnPptpServerAddress,
            config.vpnPptpServerUsername,
            config.vpnPptpServerPassword,
            config.vpnPptpServerName,function(err){
                if(err)console.log(err)
                s.startPptpVpnConnection(config.vpnPptpServerName)
            }
        )
    }
}
