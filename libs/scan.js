module.exports = function(s,config){
    const isPortReachable = require('is-port-reachable');
    const netList = require('network-list');
    var os = require('os');

    s.getBaseAddressFromIP = function(ip){
        var baseip = ip.split('.')
        baseip.splice(-1,1)
        return baseip.join('.')
    }
    s.networkInterfaces = {}
    s.loadNetworkInterfaces = function(){
        var ifaces = os.networkInterfaces();
        s.networkInterfaces = {}
        Object.keys(ifaces).forEach(function (ifname) {
            var interfaceIps = ifaces[ifname]
            if(s.isWindows)interfaceIps = interfaceIps.reverse()
            s.networkInterfaces[ifname] = interfaceIps
        })
        return s.networkInterfaces
    }
    s.getBaseAddresses = function(callback){
        var ifaces = s.loadNetworkInterfaces()
        var foundIps = []
        Object.keys(ifaces).forEach(function (ifname) {
          var alias = 0;
          var interfaceIps = ifaces[ifname]
          interfaceIps.forEach(function (iface) {
            if ('IPv4' !== iface.family || iface.internal !== false) {
              // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
              return;
            }

            if (alias >= 1) {
              // this single interface has multiple ipv4 addresses
              foundIps.push(s.getBaseAddressFromIP(iface.address))
            } else {
              // this interface has only one ipv4 adress
              foundIps.push(s.getBaseAddressFromIP(iface.address))
            }
            ++alias;
          });
        });
        callback(foundIps)
        return foundIps
    }

    s.checkHost = function(host,callback){
        //host = '192.168.1.34'
        var has = {
            openWebPort : null,
            openRtspPort : null
        }
        var checkRTSP = function(innerCallback){
            if(config.proxyRTSP === true){
                isPortReachable(config.defaultRtspPort, {host: host,timeout:1000}).then(openPort => {
                    has.openRtspPort = openPort
                    innerCallback()
                })
            }else{
                innerCallback()
            }
        }
        var checkHTTP = function(innerCallback){
            if(config.proxyHTTP === true){
                isPortReachable(config.defaultHostPort, {host: host,timeout:1000}).then(openPort => {
                    has.openWebPort = openPort
                    innerCallback()
                })
            }else{
                innerCallback()
            }
        }
        checkRTSP(function(){
            checkHTTP(function(){
                callback(has)
            })
        })
    }

    s.scanNetworkDevices = function(baseIP,callback){
        var foundDevices = []
        var foundRtspDevices = []
        var foundHttpDevices = []
        netList.scan({
            ip: baseIP
        }, (err, arr) => {
            if(arr && arr[0]){
                var aliveDevices = []
                var checkedDevices = 0
                arr.forEach(function(device,n){
                    if(device.alive){
                        aliveDevices.push(device)
                    }
                })
                aliveDevices.forEach(function(device,n){
                    s.checkHost(device.ip,function(has){
                        device = Object.assign(device,has)
                        var cleanDeviceObject = s.createCleanDeviceObject(device)
                        foundDevices.push(cleanDeviceObject)
                        ++s.numberOf.devices
                        if(has.openWebPort){
                            foundHttpDevices.push(cleanDeviceObject)
                            ++s.numberOf.httpDevices
                        }
                        if(has.openRtspPort){
                            foundRtspDevices.push(cleanDeviceObject)
                            ++s.numberOf.rtspDevices
                        }
                        if(checkedDevices >= aliveDevices.length - 1){
                            callback({
                                devices: foundDevices,
                                rtspDevices: foundRtspDevices,
                                httpDevices: foundHttpDevices,
                            })
                        }
                        ++checkedDevices
                    })
                })
            }else{
                callback({
                    devices: foundDevices,
                    rtspDevices: foundRtspDevices,
                    httpDevices: foundHttpDevices,
                })
            }
        })
    }
    s.currentlyScanning = false
    s.numberOf = {
        devices: 0,
        rtspDevices: 0,
        httpDevices: 0,
    }
    s.getAllCamerasInNetwork = function(){
        if(!s.currentlyScanning){
            s.getBaseAddresses(function(foundBases){
                s.scannedNetworks = {}
                s.allDevices = {}
                s.numberOf = {
                    devices: 0,
                    rtspDevices: 0,
                    httpDevices: 0,
                }
                s.currentlyScanning = true
                s.sendCurrentlyScanningStatus()
                var maxNumberOfIterations = foundBases.length
                var currentIteration = 1
                var checkBaseIp = function(){
                    var baseIP = foundBases[currentIteration - 1]
                    if(!s.scannedNetworks[baseIP])s.scannedNetworks[baseIP] = {}
                    s.scanNetworkDevices(baseIP,function(found){
                        found.devices.forEach(function(device,n){
                            var deviceInMemory = s.allDevices[device.ip] || device
                            if(device.openRtspPort){
                                var attachedRtspPort = config.startingRtspPort + n
                                device.rtspProxy = deviceInMemory.rtspProxy || s.attachProxy(attachedRtspPort,device.ip,config.defaultRtspPort)
                            }
                            if(device.openWebPort){
                                var attachedHostPort = config.startingHostPort + n
                                device.httpProxy = deviceInMemory.httpProxy || s.attachProxy(attachedHostPort,device.ip,config.defaultHostPort)
                            }
                            device.proxyRtspPort = attachedRtspPort
                            device.proxyHttpPort = attachedHostPort
                            var lastIpPart = device.ip.split('.')
                            lastIpPart = lastIpPart[lastIpPart.length - 1]
                            var cleanDeviceObject = s.createCleanDeviceObject(device)
                            s.scannedNetworks[baseIP][lastIpPart] = cleanDeviceObject
                            s.allDevices[device.ip] = cleanDeviceObject
                        })
                        //check another
                        if(currentIteration <= maxNumberOfIterations && foundBases[currentIteration]){
                            ++currentIteration
                            checkBaseIp()
                        }else{
                            s.currentlyScanning = false
                            var txData = s.getScannerInfo()
                            txData.f = 'scannedNetwork'
                            s.sendToConnectedClients(txData)
                        }
                        s.sendCurrentlyScanningStatus()
                    })
                }
                checkBaseIp()
            })
        }
    }
    s.getScannerInfo = function(){
        return {
            currentlyScanning : s.currentlyScanning,
            numberOf : s.numberOf,
            allDevices : s.allDevices,
        }
    }
    s.sendCurrentlyScanningStatus = function(){
        s.sendToConnectedClients({
            f: 'currentlyScanningStatus',
            currentlyScanning: s.currentlyScanning,
            numberOf: s.numberOf
        })
    }
    s.createCleanDeviceObject = function(device){
        return {
            "ip": device.ip,
            "hostname": device.hostname,
            "mac": device.mac,
            "vendor": device.vendor,
            "openRtspPort": device.openRtspPort,
            "openWebPort": device.openWebPort,
            "proxyRtspPort": device.proxyRtspPort,
            "proxyHttpPort": device.proxyHttpPort,
        }
    }
}
